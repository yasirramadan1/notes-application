package com.example.notes.presintation.feature.notes.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.domain.feature.notes.model.Note
import com.example.domain.feature.notes.usecase.AddNoteUseCase
import com.example.domain.feature.notes.usecase.DeleteNoteUseCase
import com.example.domain.feature.notes.usecase.GetNotesUseCase
import com.example.domain.feature.notes.usecase.UpdateNoteUseCase
import com.example.notes.presintation.feature.notes.NotesVOMapper
import com.example.notes.presintation.feature.notes.state.NotesViewState
import com.example.notes.presintation.feature.notes.vo.NoteVO
import cz.eman.kaal.domain.result.Result
import cz.eman.kaal.presentation.viewmodel.BaseViewModel
import kotlinx.coroutines.launch


class NotesViewModel(
    private val getNotesUseCase: GetNotesUseCase,
    private val addNoteUseCase: AddNoteUseCase,
    private val updateNoteUseCase: UpdateNoteUseCase,
    private val deleteNoteUseCase: DeleteNoteUseCase
) : BaseViewModel() {

    /**
     * represents different states of notes fragment
     */
    val state: MutableLiveData<NotesViewState> = MutableLiveData()

    /**
     * list of fetched notes.
     */
    private var noteList: List<NoteVO>? = null

    val selectedNote = MutableLiveData<NoteVO>()

    /**
     * Note to add text.
     */
    // TODO: saved here in view model just to survive configuration changed
    // TODO: will be removed when using android data binding
    private var noteToAddTitle: String? = null


    /**
     * Gets all notes
     */
    private fun getAllNotes(showLoading: Boolean) {
        if (showLoading) {
            state.value = NotesViewState.Loading
        }

        viewModelScope.launch {
            when (val result = getNotesUseCase.invoke(Unit)) {
                is Result.Success -> {

                    noteList = NotesVOMapper.mapFrom(result.data)

                    noteList?.let {
                        state.value = NotesViewState.Loaded(it)
                    }
                }

                is Result.Error -> {
                    state.value = NotesViewState.Error(result.error.message, noteList)
                }
            }
        }
    }

    /**
     * Add a note
     */
    fun addNote() {
        if (noteToAddTitle != null) {
            state.value = NotesViewState.Loading

            viewModelScope.launch {
                when (val result = addNoteUseCase.invoke(noteToAddTitle!!)) {
                    is Result.Success -> {
                        state.value = NotesViewState.NoteAdded
                        reload()
                    }

                    is Result.Error -> {
                        state.value = NotesViewState.Error(result.error.message, noteList)
                    }
                }
            }
        }
    }

    /**
     * update a note
     *
     * @param title updated note text.
     */
    fun updateNote(title: String?) {
        state.value = NotesViewState.Loading

        val note = selectedNote.value
        note?.title = title

        viewModelScope.launch {
            when (val result = updateNoteUseCase.invoke(Note(id = note!!.id, title = note.title))) {
                is Result.Success -> {
                    state.value = NotesViewState.NoteUpdated

                    reload()
                }

                is Result.Error -> {
                    state.value = NotesViewState.Error(result.error.message, noteList)
                }
            }
        }
    }

    /**
     * delete a note
     */
    fun deleteNote() {
        val noteId = selectedNote.value?.id
        noteId?.let {

            viewModelScope.launch {
                when (val result = deleteNoteUseCase.invoke(it)) {
                    is Result.Success -> {
                        state.value = NotesViewState.NoteDeleted

                        reload()
                    }

                    is Result.Error -> {
                        state.value = NotesViewState.Error(result.error.message, noteList)
                    }
                }
            }

        }
    }

    fun reload() {
        getAllNotes(false)
    }

    fun load() {
        if (!hasData()) {
            getAllNotes(true)
        }
    }

    fun setSelectedNote(note: NoteVO) {
        selectedNote.value = note
    }

    private fun hasData(): Boolean = !noteList.isNullOrEmpty()

    fun setNoteToAddTitle(title: String) {
        noteToAddTitle = title
    }
}